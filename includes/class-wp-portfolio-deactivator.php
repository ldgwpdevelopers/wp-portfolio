<?php
/**
 * Fired during plugin activation
 *
 * @since      1.0.0
 *
 * @package    WP_Portfolio
 * @subpackage WP_Portfolio/includes
 */
class WP_Portfolio_Deactivator {
	public static function deactivate() {
		global $wp_rewrite;
		$wp_rewrite->flush_rules();
	}
}
?>