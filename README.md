# README #

WP Portfolio is a plugin that allows you to handle projects as posts and organize them with their own separate project categories (hierarchical supported)

### What is this repository for? ###

This repository is for keeping track of the changes made to the code of the WP Portfolio wordpress plugin.

Version 1.0

### How do I get set up? ###

Set Up

To install this plugin just download the zip folder and upload it through the wordpress dashboard in the add new plugin screen. 
Then activate it.

Configuration

Create projects and project categories as you normally would create posts and post categories.

Dependencies

There are no dependencies

### Who do I talk to? ###

wpdev@lobodeguerra.net