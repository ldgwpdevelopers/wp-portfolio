<?php
/**
 * @package wp_portfolio
 * @version 1.0
 */
/*
Plugin Name: WP Portfolio
Plugin URI: 
Description: This plugin will let you manage your portfolios and projects as posts and categories.
Author: Isaac L. Félix
Version: 1.0
Author URI: 
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: wp-portfolio
Domain Path: /languages
*/

// Plugin activation
function wp_portfolio_activatation() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-portfolio-activator.php';
	WP_Portfolio_Activator::activate();
}
register_activation_hook( __FILE__, 'wp_portfolio_activatation');

// Plugin deactivation
function wp_portfolio_deactivatation() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-portfolio-deactivator.php';
	WP_Portfolio_Deactivator::deactivate();
}
register_deactivation_hook( __FILE__, 'wp_portfolio_deactivatation');

// Plugin uninstalling
function wp_portfolio_uninstalling() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-portfolio-uninstaller.php';
	WP_Portfolio_Uninstaller::uninstall();
}
register_uninstall_hook(__FILE__, 'wp_portfolio_uninstalling');


function wp_portfolio_register_custom_taxonomy() {
	// Arguments
	$args_raw = array(
		'labels' => array(
			'name' => __('Project Categories'),
			'singular_name' => __('Project Category'),
			'menu_name' => __('Categories')
		),
		'description' => __('Organize your projects by category'),
		'rewrite' => array(
			'hierarchical' => true,
			'slug' => 'projects'
		),
		'hierarchical' => true
	);
	// Filter arguments
	$args = apply_filters('wp_portfolio_register_custom_taxonomy_args', $args_raw);
	
	// Register taxonomy
	register_taxonomy('wp_project_category', 'wp_project', $args);
}
add_action('init', 'wp_portfolio_register_custom_taxonomy');

function wp_portfolio_register_custom_post_type() {
	// Arguments
	$args_raw = array(
		'public' => true,
		'labels' => array(
			'name' => __('Projects'),
			'singular_name' => __('Project'),
			'menu_name' => __('Projects')
		),
		'description' => __('Create your projects as posts'),
		'menu_icon' => 'dashicons-portfolio',
		'rewrite' => array(
			'slug' => 'project',
			'with_front' => true
		),
		'has_archive' => 'projects',
        'supports' => array('title', 'editor', 'thumbnail')
	);

	// Filter arguments
	$args = apply_filters('wp_portoflio_register_custom_post_type_args', $args_raw);
	
	// Register post type
	register_post_type('wp_project', $args);
}
add_action('init', 'wp_portfolio_register_custom_post_type');

// Make sure that all links on the site, include the related texonomy terms
add_filter( 'post_type_link', 'wp_portfolio_permalinks', 10, 2 );
function wp_portfolio_permalinks( $permalink, $post ) {
	if ($post->post_type !== 'wp_project')
		return $permalink;

	$terms = get_the_terms($post->ID, 'wp_project_category');
	
	if (!$terms)
		return str_replace('%wp_project_category%/', '', $permalink);
	$post_terms = array();
	foreach ($terms as $term)
		$post_terms[] = $term->slug;
	return str_replace('%wp_project_category%', implode('/', $post_terms), $permalink);
}
?>